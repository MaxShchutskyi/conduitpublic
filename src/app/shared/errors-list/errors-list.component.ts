import { Component, Input } from '@angular/core';
import { Errors } from '../../core';

@Component({
  selector: 'app-errors-list',
  templateUrl: './errors-list.component.html',
  styleUrls: ['./errors-list.component.scss']
})
export class ErrorsListComponent {
  formattedErrors: Array<string> = [];

  @Input()
  set errors(errorsList: Errors) {
    this.formattedErrors = Object.keys(errorsList.errors || {}).map(key => `${key} ${errorsList.errors[key]}`);
  }

  get errorList() {
    return this.formattedErrors;
  }

}
