import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { User, AuthService } from '../core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  title: String = 'Your Settings';
  editForm: FormGroup;
  user: User = {} as User;
  isSubmiting = false;
  errors: Object = {};
  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) { 
    this.editForm = this.fb.group({
      'image': '',
      'username': '',
      'bio': '',
      'email': '',
      'password': '',
    });
  }

  ngOnInit() {
   Object.assign(this.user, this.auth.getCurrentUser());
   this.editForm.patchValue(this.user);
  }

  logOut() {
    this.auth.removeAuth();
    this.router.navigateByUrl('/');
  }

  updateForm() {
    this.isSubmiting = true;
    this.updateUser(this.editForm.value);
    this.auth.update(this.user)
    .subscribe(data => {
      console.log('datka', data);
    },
    err => {
      this.errors = err;
      this.isSubmiting = false;
    });
  }

  updateUser(values: Object) {
    Object.assign(this.user, values);
  }
}
