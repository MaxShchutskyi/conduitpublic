import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import {  AuthService } from '../core/services/auth.service';
import { Errors } from '../core/models/errors.model';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  authForm: FormGroup;
  isSubmitting = false;
  user: Object;
  authType: String = '';
  errors: Errors = {errors: {}};
  link: String = '';
  title: String = '';

  constructor(private builder: FormBuilder, private auth: AuthService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.authForm = this.builder.group({
      'email': ['maxshutskiy@ukr.net'],
      'password': ['80612575745max'],
    }, {validator: this.checkPasswords});

    this.route.url.subscribe(data => {
      this.authType = data[data.length - 1].path;
      this.title = (this.authType === 'login') ? 'Sing in' : 'Sing up';
      this.link = (this.authType === 'login') ? '/login' : '';
      if (this.authType === 'register') {
        this.authForm.addControl('username', new FormControl);
        this.authForm.addControl('confirmPass', new FormControl());
      }

    });

  }

  checkPasswords(authForm: FormGroup) {
    if (authForm.get('confirmPass')) {
      const pass = authForm.get('password').value;
      const confirmPass = authForm.get('confirmPass').value;

      return pass === confirmPass ? null : { notSame: true };
    } else {
      return false;
    }
}

  submitForm() {
    this.isSubmitting = true;
    const credentials = {};
    Object.keys(this.authForm.value).map(key => {
      if (key !== 'confirmPass') {
        credentials[key] = this.authForm.value[key];
      }
    }) ;
    this.auth
    .attemptAuth('/users' + this.link, credentials)
    .subscribe(
      data => {
        this.router.navigateByUrl('/');
      },
      err => {
        this.errors = err;
        this.isSubmitting = false;
      }
    );
  }
}
