import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable, Subject, BehaviorSubject, ReplaySubject } from 'rxjs';
import { map, distinctUntilChanged, take } from 'rxjs/operators';
import { User } from '../models/user.model';
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

  private isAutorisedSubject = new ReplaySubject<boolean>(1);
  public isAutorised = this.isAutorisedSubject.asObservable();

  constructor(private apiService: ApiService, private jwt: JwtService) { }

  setAuth(user: User) {
    this.jwt.saveToken(user.token);
    this.currentUserSubject.next(user);
    this.isAutorisedSubject.next(true);
  }

  removeAuth() {
    this.jwt.destroyToken();
    this.currentUserSubject.next({} as User);
    this.isAutorisedSubject.next(false);
  }

  update(user: User): Observable<User> {
    return this.apiService.put('/user', { user }).pipe(map(data => {
      this.currentUserSubject.next(data.user);
      return data.user;
    }));
  }

  attemptAuth(path, credentials): Observable<User> {
    return this.apiService.post(path, {user: credentials})
      .pipe(map(
      data => {
        this.setAuth(data.user);
        return data;
      }
    ));
  }

  getCurrentUser(): User {
    return this.currentUserSubject.value;
  }

  populate() {
    if (this.jwt.getToken()) {
      this.apiService.get('/user')
      .subscribe(
        data => {
          return this.setAuth(data.user);
        },
        err => {
          return this.removeAuth();
        }
      );
    } else {
      this.removeAuth();
    }
  }
}
