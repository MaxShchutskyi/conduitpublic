import { Directive, TemplateRef, ViewContainerRef, Input, OnInit } from '@angular/core';
import { AuthService } from '../core';

@Directive({
  selector: '[appShowAuth]'
})
export class ShowAuthDirective implements OnInit {
  condition: boolean;
  constructor(private auth: AuthService, private templateRef: TemplateRef<any>,
              private viewContainer: ViewContainerRef) { }

    ngOnInit(): void {
      this.auth.isAutorised.subscribe(isAuth => {
        if (isAuth && this.condition || !isAuth && !this.condition) {
          this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
          this.viewContainer.clear();
        }
      });
    }

    @Input() set appShowAuth (condition: boolean) {
      this.condition = condition;
    }
}


