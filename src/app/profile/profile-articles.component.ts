import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route } from '@angular/router';
import { tap, concatMap } from 'rxjs/operators';
import { Profile, AuthService, User, ArticleListConfig } from '../core';

@Component({
  selector: 'app-profile-articles',
  templateUrl: './profile-articles.component.html',
})
export class ProfileArticlesComponent implements OnInit {
  profile: Profile;
  currentUser: User;
  isUser: boolean;
  articlesConfig: ArticleListConfig = {
    type: 'all',
    filters: {}
  };
  constructor(private active: ActivatedRoute, private authService: AuthService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.parent.data.subscribe(
      (data: {profile: Profile}) => {

        this.profile = data.profile;
        this.articlesConfig = {
          type: 'all',
          filters: {}
        }; // Only method I found to refresh article load on swap
        this.articlesConfig.filters.author = this.profile.username;
      }
    );
  }

}