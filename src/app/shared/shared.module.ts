import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { ArticleListComponent, ArticleMetaComponent, ArticlePreviewComponent } from './article-helpers';
import { ErrorsListComponent } from './errors-list/errors-list.component';
import { ShowAuthDirective } from './show-auth.directive';
import { FavoriteButtonComponent, FollowButtonComponent} from './buttons';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule
  ],
  declarations: [
    ArticleListComponent,
    ArticleMetaComponent,
    ArticlePreviewComponent,
    ErrorsListComponent,
    ShowAuthDirective,
    FavoriteButtonComponent,
    FollowButtonComponent
  ],
  exports: [
    ArticleListComponent,
    ArticleMetaComponent,
    ArticlePreviewComponent,
    CommonModule,
    FavoriteButtonComponent,
    FollowButtonComponent,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ErrorsListComponent,
    RouterModule,
    ShowAuthDirective
  ]
})
export class SharedModule {}