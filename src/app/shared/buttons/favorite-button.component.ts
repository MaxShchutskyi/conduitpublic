import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Article, AuthService, ArticlesService } from '../../core';
import { concatMap, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { of } from 'rxjs';

@Component({
  selector: 'app-favorite-button',
  templateUrl: './favorite-button.component.html'
})
export class FavoriteButtonComponent implements OnInit {

  @Input() article: Article;
  @Output() toggle = new EventEmitter<boolean>();
  isSubmiting = false;
  constructor(private authService: AuthService, private router: Router, private articlesService: ArticlesService) { }

  ngOnInit() {
  }

  toggleFavorite() {
    this.isSubmiting = true;
    this.authService.isAutorised.pipe(concatMap(
      (autorised) => {
        if (!autorised) {
          this.router.navigateByUrl('/login');
          return of(null);
        }

        if (!this.article.favorited) {
          return this.articlesService.favorite(this.article.slug)
          .pipe(tap(
            data => {
              this.isSubmiting = false;
              this.toggle.emit(true);
            },
            err => this.isSubmiting = false
          ));
        } else   if (this.article.favorited) {
          return this.articlesService.unfavorite(this.article.slug)
            .pipe(tap(
              data => {
                this.isSubmiting = false;
                this.toggle.emit(false);
            },
                err => this.isSubmiting = false
          ));
        }
      }

    )).subscribe();
    console.log(this.article);
  }
}