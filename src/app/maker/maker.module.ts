import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MakerRoutingModule } from './maker-routing.module';
import { MakerComponent } from './maker.component';
import { SharedModule } from '../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MakerResolverService } from './maker-resolver.service';

@NgModule({
  imports: [
    CommonModule,
    MakerRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatButtonModule,
  ],
  declarations: [MakerComponent],
  providers: [MakerResolverService]
})
export class MakerModule { }
