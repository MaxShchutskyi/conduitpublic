import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { EditComponent } from './edit.component';
import { AuthGuard } from '../core/services/auth.guard';

const routes: Routes = [
  {
      path: '',
      component: EditComponent,
      canActivate: [AuthGuard]
  }
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class EditRoutingModule { }
