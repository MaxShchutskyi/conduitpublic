import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './interceptors/token.interceptor';

import {
  ApiService,
  AuthGuard,
  JwtService,
  AuthService,
} from './services';


@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    ApiService,
    AuthGuard,
    JwtService,
    AuthService
  ],
  declarations: []
})
export class CoreModule { }
