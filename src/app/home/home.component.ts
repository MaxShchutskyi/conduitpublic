import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { TagsService } from '../core/services/tags.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isAutorised: boolean;
  tagsLoaded = false;
  tags: Array<string>;
  listParams = {
    type: 'all',
    filters: {}
  };

  constructor(private actr: ActivatedRoute, private tagsService: TagsService, private router: Router) {
    this.actr.data.pipe(map(data => data.isAutorised) ).subscribe(res => {
      this.isAutorised = res;
    });
  }

  ngOnInit() {
    if (this.isAutorised) {
      this.setList('feed');
    } else {
      this.setList('all');
    }

    this.tagsService.getAll().subscribe(res => {
      this.tagsLoaded = true;
      this.tags = res;
    });
  }

  setList(type: string, filters: Object = {}) {
    if (!this.isAutorised && type === 'feed') {
      this.router.navigateByUrl('/login');
      return;
    }
    this.listParams = {type: type, filters: filters};
    // console.log(this.listParams);
  }

}
