import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { ProfileService } from '../core/services/profile.service';
import { Profile } from '../core';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ProfileResolverService implements Resolve<Profile> {

  constructor(private profileService: ProfileService, private router: Router) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    return this.profileService.getProfile(route.params['username']).pipe(catchError((err) => this.router.navigateByUrl('/')));
  }
}
