import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Article, AuthService, User, ArticlesService, Comment, Errors } from '../core';
import { CommentsService } from '../core/services/comments.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-articles',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  article: Article;
  currentUser: User;
  isModify: boolean;
  comments: Comment[];
  isSubmiting = false;
  commentFormErrors = {};
  commentControl = new FormControl();
  constructor(private route: ActivatedRoute, private authService: AuthService, private articleService: ArticlesService,
               private router: Router, private commentsService: CommentsService) { }

  ngOnInit() {
    this.route.data.subscribe((data: {article: Article}) => {
      this.article = data.article;
      console.log(this.article);
    });

    this.authService.currentUser.subscribe((user: User) => {
      this.currentUser = user;
      this.isModify = this.currentUser.username === this.article.author.username;
    });

    this.populateComments();
  }

  populateComments() {
    this.commentsService.getComments(this.article.slug).subscribe(comments => this.comments = comments);
  }
  deleteArticle() {
    this.articleService.delete(this.article.slug).subscribe((resolve) => {
      this.router.navigateByUrl('/');
    });
  }

  onToggleFollow(follow: boolean) {
    this.article.author.following = follow;
  }

  onToggleFavorite(favorited: boolean) {
    this.article['favorited'] = favorited;
    if (favorited) {
      this.article.favoritesCount++;
    } else {
      this.article.favoritesCount--;
    }
  }

  addComment() {
    this.isSubmiting = true;
    this.commentFormErrors = {};
    const commentBody = this.commentControl.value;

    this.commentsService.addComment(this.article.slug, commentBody).subscribe((comment) => {
      this.isSubmiting = false;
      this.comments.unshift(comment);
      this.commentControl.reset('');
    },
    err => {
      this.isSubmiting = false;
      this.commentFormErrors = err;
    });
  }

  onDeleteComment(comment) {
    this.isSubmiting = true;
    this.commentsService.deleteComment(this.article.slug, comment.id).subscribe((data) => {
      this.isSubmiting = false;
      this.comments = this.comments.filter((item) => item !== comment);
    });
  }
}
