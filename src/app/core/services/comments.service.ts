import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { Comment } from '../models';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private api: ApiService) { }

  getComments(slug: string): Observable<Comment[]> {
    return this.api.get('/articles/' + slug + '/comments').pipe(map(data => data.comments));
  }

  addComment(slug, commentBody): Observable<Comment> {
    return this.api.post('/articles/' + slug + '/comments', {comment: {body: commentBody}}).pipe(map(data => data.comment));
  }

  deleteComment(slug, id): Observable<any> {
    return this.api.delete('/articles/' + slug + '/comments/' + id);
  }
}

