import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ArticleListConfig, Article } from '../models';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(private api: ApiService) { }

  query(config: ArticleListConfig): Observable<{articles: Article[], articlesCount: number}> {
    const params = {};

    Object.keys(config.filters).forEach((key) => {
      params[key] = config.filters[key];
    });

    const str = (config.type === 'feed') ? '/feed' : '';
    return this.api.get('/articles' + str, new HttpParams({fromObject: params}));
  }
  get(slug): Observable<Article> {
    return this.api.get('/articles/' + slug).pipe(map(data => data.article));
  }
  favorite(slug): Observable<Article> {
    return this.api.post('/articles/' + slug + '/favorite');
  }
  unfavorite(slug): Observable<Article> {
    return this.api.delete('/articles/' + slug + '/favorite');
  }
  save(article: Article, slug): Observable<Article> {
    if(slug) {
      return this.api.put('/articles/' + slug, article);
    } else {
      return this.api.post('/articles', article);
    }
  }
  delete(slug): Observable<any> {
    return this.api.delete('/articles/' + slug);
  }
}
