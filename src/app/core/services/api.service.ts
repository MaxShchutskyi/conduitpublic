import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const headers = new HttpHeaders({ 'Authorization': `Token ${window.localStorage['jwtToken']}` });
const options = { headers: headers };

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private formatErrors(error: any) {
    return  throwError(error.error);
  }
  constructor(private http: HttpClient) { }

  get (path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(`${environment.api_url}${path}`, { params });
  }

  post(path: string, body: Object = {}): Observable<any> {
    console.log(`${environment.api_url}${path}`);
    return this.http.post(
      `${environment.api_url}${path}`,
      JSON.stringify(body), httpOptions
    ).pipe(catchError(this.formatErrors));
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(`${environment.api_url}${path}`, JSON.stringify(body))
    .pipe(catchError(this.formatErrors));
  }
  delete(path: string): Observable<any> {
    return this.http.delete(`${environment.api_url}${path}`)
    .pipe(catchError(this.formatErrors));
  }
}

