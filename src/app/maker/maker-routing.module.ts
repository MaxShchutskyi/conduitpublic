import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MakerComponent } from './maker.component';
import { MakerResolverService } from './maker-resolver.service';
import { AuthGuard } from '../core';

const routes: Routes = [
  {
    path: '',
    component: MakerComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':slug',
    component: MakerComponent,
    canActivate: [AuthGuard],
    resolve: {
      article: MakerResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MakerRoutingModule { }
