import { Component, OnInit, Input } from '@angular/core';
import { ArticlesService, ArticleListConfig, Article } from '../../../core';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss']
})
export class ArticleListComponent implements OnInit {

  constructor(private articlesService: ArticlesService) { }
  @Input() limit: number;
  @Input()
  set config(config: ArticleListConfig) {
    if (config) {
      // console.log(config);
      this.query = config;
      this.currentPage = 1;
      this.runQuery();
    }
  }

  query: ArticleListConfig;
  results: Article[];
  articlesCount: Number;
  isLoad = false;
  currentPage = 1;
  totalPages: Array<number> = [];
  ngOnInit() {
  }

  runQuery() {
    this.isLoad = true;
    this.results = [];

    if (this.limit) {
      this.query.filters.limit = this.limit;
      this.query.filters.offset = (this.limit * (this.currentPage - 1));
    }

    this.articlesService.query(this.query).subscribe(res => {
      // console.log(res);
      this.isLoad = false;
      this.results = res.articles;

      this.totalPages = Array.from(new Array(Math.ceil(res.articlesCount / this.limit)), (val, index) => index + 1);
    });

  }

  setPage(page: number) {
    if (this.currentPage !== page) {
      this.currentPage = page;
    this.runQuery();
    }
  }
}
