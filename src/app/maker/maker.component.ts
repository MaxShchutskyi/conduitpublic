import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Errors, Article, ArticlesService } from '../core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-maker',
  templateUrl: './maker.component.html',
  styleUrls: ['./maker.component.scss']
})
export class MakerComponent implements OnInit {
  errors: Object = {};
  formMaker: FormGroup;
  tagField = new FormControl();
  article: Article = {} as Article;
  isSubmiting = false;
  constructor(private fb: FormBuilder, private articlesService: ArticlesService, private router: Router, private route: ActivatedRoute) {
    this.formMaker = this.fb.group({
      title: '',
      description: '',
      body: ''
    });
    this.article.tagList = [];
   }

  ngOnInit() {
    this.route.data.subscribe((data: {article: Article}) => {
      if (data.article) {
        this.article = data.article;
        this.formMaker.patchValue(data.article);
      }
    });
  }

  addTag() {
    const tag = this.tagField.value;
    
    if (this.article.tagList.indexOf(tag) < 1) {
      this.article.tagList.push(tag);
    }
    this.tagField.reset('');
  }

  removeTag(tag) {
    this.article.tagList = this.article.tagList.filter( item => item !== tag );
  }

  submitForm() {
    this.isSubmiting = true;
    this.updateArticle(this.formMaker.value);
    console.log(this.article);
    this.articlesService.save(this.article, this.article.slug).subscribe((article) => {
      this.isSubmiting = false;
      this.router.navigateByUrl('/article/' + article.slug);
    },
    err => {
      this.isSubmiting = false;
      this.errors = err;
    });
  }
  
  updateArticle(article: Article) {
    Object.assign(this.article, article);
  }
}
