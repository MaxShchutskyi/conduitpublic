import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { Profile } from '../models';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private apiService: ApiService) { }

  getProfile(slug: string): Observable<Profile> {
    return this.apiService.get(`/profiles/${slug}`).pipe(map((data: {profile: Profile}) => data.profile));
  }

  follow(username: string) {
    return this.apiService.post(`/profiles/${username}/follow`).pipe(map((data: {profile: Profile}) => data.profile));
  }
  unfollow(username: string) {
    return this.apiService.delete(`/profiles/${username}/follow`).pipe(map((data: {profile: Profile}) => data.profile));
  }
}
