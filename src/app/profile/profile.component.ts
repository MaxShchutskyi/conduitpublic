import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { tap, concatMap } from 'rxjs/operators';
import { Profile, AuthService, User } from '../core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profile: Profile;
  currentUser: User;
  isUser: boolean;

  constructor(private active: ActivatedRoute, private authService: AuthService) { }

  ngOnInit() {
    this.active.data.pipe(
      concatMap((data: { profile: Profile }) => {
      this.profile = data.profile;
      return this.authService.currentUser.pipe(tap(
        (userData: User) => {
          this.currentUser = userData;
          this.isUser = (this.profile.username === this.currentUser.username);
        }
      ));
    })
    ).subscribe();
  }

  onToggleFollow(follow: boolean) {
    this.profile.following = follow;
  }

}
