import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleComponent } from './article.component';
import { ArticleRoutingModule } from './article-routing.module';
import { ArticleResolverService } from './article-resolver.service';
import { SharedModule } from '../shared';
import { ArticleCommentComponent } from './article-comment.component';


@NgModule({
  imports: [
    CommonModule,
    ArticleRoutingModule,
    SharedModule
  ],
  declarations: [ArticleComponent, ArticleCommentComponent],
  providers: [ArticleResolverService]
})
export class ArticleModule { }
