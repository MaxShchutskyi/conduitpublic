import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core';
import { User } from '../../core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  title: String = 'conduit';
  currentUser: User;
  image: String = '../../assets/img/Unknown.png';
  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.auth.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
        if (this.currentUser.image) {
          this.image = this.currentUser.image;
        }
        // console.log(this.currentUser);
      }
    );
  }

}
