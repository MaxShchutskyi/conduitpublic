import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Article, ArticlesService, AuthService } from '../core';
import { catchError, map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class MakerResolverService implements Resolve<Article> {

  constructor(private articlesService: ArticlesService, private router: Router, private authService: AuthService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    return this.articlesService.get(route.params['slug']).pipe(map(
      article => {
        if (this.authService.getCurrentUser().username === article.author.username) {
          return article;
        } else {
          this.router.navigateByUrl('/');
        }
      }
    ),
    catchError((err) => this.router.navigateByUrl('/')));
  }
}
