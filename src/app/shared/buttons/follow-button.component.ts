import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Profile, AuthService } from '../../core';
import { concatMap, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { ProfileService } from 'src/app/core/services/profile.service';

@Component({
  selector: 'app-follow-button',
  templateUrl: './follow-button.component.html'
})
export class FollowButtonComponent implements OnInit {

  @Input() profile: Profile;
  @Output() toggle = new EventEmitter<boolean>();

  isSubmiting = false;
  constructor(private authService: AuthService, private router: Router, private profileService: ProfileService) { }

  ngOnInit() {
      console.log(this.profile);
  }

  toggleFollow() {
    this.isSubmiting = true;

    this.authService.isAutorised.pipe(concatMap(
      (autorized) => {
        if (!autorized) {
          this.router.navigateByUrl('/login');
          return of(null);
        }

        if (!this.profile.following) {
          return this.profileService.follow(this.profile.username).pipe(tap(
            (data) => {
              this.isSubmiting = false;
              this.toggle.emit(true);
            },
            err => this.isSubmiting = false
          ));
        } else if (this.profile.following) {
          return this.profileService.unfollow(this.profile.username).pipe(tap(
            (data) => {
              this.isSubmiting = false;
              this.toggle.emit(false);
            },
            err => this.isSubmiting = false
          ));
        }
      }
    )).subscribe();
  }
}
