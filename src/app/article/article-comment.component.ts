import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Comment, AuthService, User } from '../core';


@Component({
  selector: 'app-article-comment',
  templateUrl: './article-comment.component.html'
})
export class ArticleCommentComponent implements OnInit {
  @Input() comment: Comment;
  @Output() deleteComment = new EventEmitter<boolean>();
  currentUser: User;
  isModify: boolean;
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.currentUser.subscribe((userData: User) => {
      this.currentUser = userData;
      this.isModify = this.currentUser.username === this.comment.author.username;
    });
  }

  deleteClicked() {
    this.deleteComment.emit(true);
  }

}
